#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def action_on():
    return "Speaker On"

def action_off():
    return "Speaker Off"
