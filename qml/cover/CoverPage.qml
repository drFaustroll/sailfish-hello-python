import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.4

CoverBackground {
    Image {
        source: "image://theme/icon-m-speaker"
    }
    Label {
        id: label
        anchors.centerIn: parent
        text: ("Speaker On/Off")
    }

    CoverActionList {
        id: coverAction

        CoverAction {
            iconSource: "image://theme/icon-m-speaker"
            onTriggered: python.call('coveractions.action_on', [], function(newstring) {
                python.call('speaker.speaker.On',['output-speaker'], function() {});
                label.text = newstring;
            });
        }

        CoverAction {
            iconSource: "image://theme/icon-m-speaker-mute"
            onTriggered: python.call('coveractions.action_off', [], function(newstring) {
                python.call('speaker.speaker.On',['output-wired_headphone'], function() {});
                label.text = newstring;
            });
        }
    }

    Python {
        id: python

        Component.onCompleted: {
        addImportPath(Qt.resolvedUrl('.'));
        importModule('coveractions', function () {});
        addImportPath(Qt.resolvedUrl('../pages/'));
        importModule('speaker', function () {});
        }
      }
    }


