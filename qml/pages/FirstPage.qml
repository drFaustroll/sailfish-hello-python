import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.4

Page {
  id: page
  allowedOrientations : Orientation.All
  PageHeader{
    id: head
    title: qsTr("Reroute sound to the port")
  } 
 
  Component {
    id: spDelegate 
      Column{
        id: col
        width: parent.width

        Button {
          width: 0.8*parent.width
          anchors.horizontalCenter: parent.horizontalCenter
          text: qsTr(name)
          color:  Theme.secondaryColor
          onClicked: {
            python.speakerOn(speaker);
          }
        }
      }
    }


  ListView {
    id: listView
    spacing: Theme.paddingLarge
    contentHeight: col.Height
    anchors.top: head.bottom 
    width: page.width
    height: page.height
    model: ListModel {
      id: listModel
    }
    delegate : spDelegate
  }
  

  Python {
    id: python
    Component.onCompleted: {
      addImportPath(Qt.resolvedUrl('.'));

      setHandler('progress', function() {
        console.log('got progress from python: ');
      });

      setHandler('finished', function(newvalue) {
      });

      importModule('speaker', function () {
        python.call('speaker.speaker.getSpeakers',[],function(result){
          for (var i=0; i<result.length; i++) {
            listModel.append(result[i]);
          }
        });
      });
    }

    function speakerOn(port) {
      call('speaker.speaker.On',[port], function() {});
    }

    onError: {
      // when an exception is raised, this error handler will be called
      console.log('python error: ' + traceback);
    }

    onReceived: {
      // asychronous messages from Python arrive here
      // in Python, this can be accomplished via pyotherside.send()
      console.log('got message from python: ' + data);
    }
  }
}
