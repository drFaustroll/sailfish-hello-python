#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pyotherside
import threading
from pulsectl import Pulse


def slow_on_function(p,sink,port):
    p.sink_port_set(sink,port)
    pyotherside.send('finished')

class Speaker:
    def __init__(self):
        self.p=Pulse()
        snk=0
        for s in self.p.sink_list():
          if s.state=='running':
             snk=s.index
        self.sink=snk
        s=self.p.sink_list()[snk]
        active=s.port_active.name
        self.ports=[ {'speaker': port.name,'name': port.description} for port in s.port_list if port.name != active] 
        self.ports.insert(0,{'speaker': active,'name':s.port_active.description})
        self.bgthread = threading.Thread()
        self.bgthread.start()

    def getSpeakers(self):
        return  self.ports 

    def On(self,port):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=slow_on_function(self.p,self.sink,port))
        self.bgthread.start()

speaker = Speaker()

